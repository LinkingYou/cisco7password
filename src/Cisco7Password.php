<?php
/*
 * This file is part of the linking-you/cisco7password package.
 *
 * (c) Frank Müller <frank.mueller@linking-you.de>
 *
 * @license MIT
 */

namespace LinkingYou\Cisco7Password;

use Exception;

class Cisco7Password
{
    const keys = [0x64, 0x73, 0x66, 0x64, 0x3b, 0x6b, 0x66, 0x6f, 0x41, 0x2c, 0x2e, 0x69, 0x79, 0x65, 0x77, 0x72, 0x6b, 0x6c, 0x64
        , 0x4a, 0x4b, 0x44, 0x48, 0x53, 0x55, 0x42, 0x73, 0x67, 0x76, 0x63, 0x61, 0x36, 0x39, 0x38, 0x33, 0x34, 0x6e, 0x63,
        0x78, 0x76, 0x39, 0x38, 0x37, 0x33, 0x32, 0x35, 0x34, 0x6b, 0x3b, 0x66, 0x67, 0x38, 0x37];

    /**
     * @param string $crypted
     * @return string
     */
    public static function decrypt7Password(string $crypted) : string
    {
        $decrypted = '';

        $s = intval(substr($crypted, 0, 2));
        $e = substr($crypted, 2);

        for ($i = 0; $i < strlen($e); $i += 2) {
            $cur = hexdec(substr($e, $i, 2)) ^ self::keys[$s++];
            $decrypted .= chr($cur);

            if ($s >= count(self::keys)) {
                $s = 0;
            }
        }

        return $decrypted;
    }

    /**
     * @param string $unencrypted
     * @return string
     * @throws Exception
     */
    public static function encrypt7Password(string $unencrypted) : string
    {
        $s = random_int(0, count(self::keys) - 1);
        $encrypted = sprintf("%02s", $s);

        for ($pos = 0; $pos < strlen($unencrypted); $pos++) {
            $encrypted .= sprintf("%02X", ord(substr($unencrypted, $pos, 1)) ^ self::keys[$s++]);

            if ($s >= count(self::keys)) {
                $s = 0;
            }
        }

        return strtoupper($encrypted);
    }
}
