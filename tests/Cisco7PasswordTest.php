<?php
/*
 * This file is part of the linking-you/cisco7password package.
 *
 * (c) Frank Müller <frank.mueller@linking-you.de>
 *
 * @license MIT
 */

namespace LinkingYou\Cisco7Password\Tests;

use Exception;
use LinkingYou\Cisco7Password\Cisco7Password;
use PHPUnit\Framework\TestCase;

class Cisco7PasswordTest extends TestCase
{

	public function testDecrypt7Password()
    {
        // Check with a password created on a cisco device
        $this->assertEquals(
            'ichbineinlangespasswortundsehrgeheimichbineinlangespasswortundsehrgeheimichbineinlangespasswortundsehrgeheimichbineinlangespasswortundsehrgeheim',
            Cisco7Password::decrypt7Password('1210061F10020201232528293D3227001717101241564A474100070B13514A50565A505D0652050F5A5E0A160F0A570A0808245F5E080A16001D191811242F372D3B2725160F130A0C5F5A50515D00061118555959545746440A481510574510060800480E0E1D2649460C10081E11030E0D242E2D263F342C1402051300454A4F5C461A1616124A5D5F4155505C0E520B')
        );
    }

    /**
     * @throws Exception
     */
    public function testEncrypt7Password()
    {
        $teststring = 'test asfasd fsad fasdfasdf äöüß asd 123123 !§$%&&/(()()=';
        $teststringEncrypted = (Cisco7Password::encrypt7Password($teststring));
        $this->assertEquals(
            $teststring,
            Cisco7Password::decrypt7Password($teststringEncrypted)
        );
    }
}
